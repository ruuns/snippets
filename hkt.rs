#![feature(generic_associated_types)]

#[derive(Default)]
struct A { value: u32 }

#[derive(Default)]
struct B { value: u8 }

#[derive(Default)]
struct VecSectionReader<T> {
    pub data: Vec<T>,
}

#[derive(Default)]
struct ByteSectionReader<T> {
    pub data: Vec<u8>,
    _p: std::marker::PhantomData<T>,
}

#[derive(Default)]
struct ReaderFamily {}

trait SectionFamily {
    type VecSectionHandler<T>;
    type ByteSectionHandler<T>;
}

impl SectionFamily for ReaderFamily {
    type VecSectionHandler<T> = VecSectionReader<T>;
    type ByteSectionHandler<T> = ByteSectionReader<T>;
}

#[derive(Default)]
struct Sections<T: SectionFamily> {
    a: T::VecSectionHandler<A>,
    b: T::ByteSectionHandler<B>,
}

trait Dispatch<F: SectionFamily> {
    type H;
    fn dispatch(sections: &Sections<F>) -> &Self::H;
}

impl<F: SectionFamily> Dispatch<F> for A {
    type H = F::VecSectionHandler<A>;

    fn dispatch(sections: &Sections<F>) -> &Self::H {
        &sections.a
    }
}

impl<F: SectionFamily> Dispatch<F> for B {
    type H = F::ByteSectionHandler<B>;

    fn dispatch(sections: &Sections<F>) -> &Self::H {
        &sections.b
    }
}

#[derive(Default)]
struct Reader {
    sections: Sections<ReaderFamily>,
}

impl Reader {
    pub fn len<T: Dispatch<ReaderFamily>>(&self) -> usize {
        T::dispatch(&self.sections).data.len()
    }
}

fn main() {
    let fr = Reader::default();
    println!("Len of A {}", fr.len::<A>());
    println!("Len of B {}", fr.len::<B>());
}
